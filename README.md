## Prerequisites

- [Erlang](https://github.com/asdf-vm/asdf-erlang) v22
- [Elixir](https://github.com/asdf-vm/asdf-elixir) v1.9
- PostgreSQL (`docker run --name postgres -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres:11`)

## Run dev server

```
mix deps.get
mix ecto.create && mix ecto.migrate
iex -S mix
```

Then check out http://localhost:4001/bashboard/system, you should see a blob of json there.

And now you can send data to your bashboard:

```bash
curl --data 1 http://localhost:4001/myuser/mydashboard/mywidget
curl --data 2 http://localhost:4001/myuser/mydashboard/mywidget
curl --data 3 http://localhost:4001/myuser/mydashboard/mywidget
curl --data 4 http://localhost:4001/myuser/mydashboard/mywidget
```

But it all looks better with [the front end](https://gitlab.com/bashboard/bashboard-front-svelte/).

## Testing

```
# normal
mix test

# Test performance with a million rows
MIX_ENV=test_million mix test --only million
```

## Deploy to production

Currently it's a mixture of the front and back ends.

- create server
- create user web
- `mkdir -p /var/lib/bashboard/back`
- `chown -R web:web /var/lib/bashboard`
- install node.js v13
- Enable systemd service
  - copy .service file from project root to server `/etc/systemd/system/bashboard-back.service`
  - `systemctl daemon-reload`
  - `systemctl start bashboard-back`
  - `systemctl enable bashboard-front`
- GitLab repo -> Settings -> CI / CD -> Variables
  - If you're going to deploy both front and back, it's easier to set these variables to group settings, not just repo.
  - `SSH_KNOWN_HOSTS` - this will be added to runner `~/.ssh/known_hosts`
  - `SSH_PRIVATE_KEY` - Generate a SSH key and put the private part here, public part to your server (both root and web users)
  - `SSH_SERVER_IP` - target server to connect to
  - `DB_NAME` - PostgreSQL connection param
  - `DB_USER` - PostgreSQL connection param
  - `DB_PASS` - PostgreSQL connection param
  - `DB_HOST` - PostgreSQL connection param
- run GitLab CD

## Why is this open source if we're trying to build a business?

We are fans of open source and it's a pretty big decision point when we choose our own software. And the target audience is developers also. While our business model is to sell quality hosting and support. Devs being devs they will of course host their own and leave us out of the income loop, so here are some considerations:

- The biggest risk of the project currently is that it gains no audience at all. OS helps spread the word.
- If there is popularity then competition will follow regardless.
- While the product is basically free for individuals, businesses will want to opt for a cared hosting solution. Plus, individuals will spread the word.
- This product/business/team is new so to mitigate risk for clients - they can always host their own setup.
- With OS you'll never know what new ideas and directions the community will come up with.
- If the hosting will prove unfundable there are more options:
    - patreon type support (our goal isn't to sell for billions but to build a great product while living well)
    - proprietary plugins like ML based insights
    - deep support for businesses
    - paid feature requests

