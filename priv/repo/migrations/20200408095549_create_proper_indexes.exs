defmodule Bashboard.Repo.Migrations.CreateProperIndexes do
  use Ecto.Migration

  def change do
    # useless old index
    drop(index(:messages, [:user, :dash, :widget]))

    # messages
    create(index(:messages, [:inserted_at]))
    create(index(:messages, [:user, :dash, :widget, :inserted_at]))
    execute("CREATE INDEX messages_typeof ON messages (jsonb_typeof(body)) INCLUDE (body)")

    # continuous aggregation tables
    for bucket <- ["minute", "hour", "day", "month"],
        type <- ["all", "numbers", "simple", "maps"] do
      create(index("messages_#{type}_#{bucket}", [:inserted_at]))
      create(index("messages_#{type}_#{bucket}", [:user, :dash, :widget, :inserted_at]))
    end
  end
end
