defmodule Bashboard.Repo.Migrations.CreateAggStrings do
  use Ecto.Migration

  def change do
    for bucket <- ["minute", "hour", "day", "month"] do
      drop(table("messages_simple_#{bucket}"))

      new_name = "messages_strings_#{bucket}"

      create table(new_name) do
        add(:user, :string, null: false)
        add(:dash, :string, null: false)
        add(:widget, :string, null: false)
        add(:body, :map)
        add(:inserted_at, :utc_datetime)
      end

      create(index(new_name, [:inserted_at]))
      create(index(new_name, [:user, :dash, :widget, :inserted_at]))
    end
  end
end
