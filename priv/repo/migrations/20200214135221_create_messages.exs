defmodule Bashboard.Repo.Migrations.CreateMessages do
  use Ecto.Migration

  def change do
    create table(:messages) do
      add(:user, :string, null: false)
      add(:dash, :string, null: false)
      add(:widget, :string, null: false)
      add(:body, :map)
      timestamps(type: :utc_datetime, updated_at: false)
    end

    create(index(:messages, [:user, :dash, :widget]))
  end
end
