defmodule Bashboard.Router.User do
  use Plug.Router
  alias Bashboard.Repo, as: Repo
  alias Bashboard.User, as: User
  alias Bashboard.UserReservartion, as: UserReservartion

  plug(:match)
  plug(:dispatch)

  get "/:user" do
    dashboards =
      User.get_dashboards(conn.path_info)
      |> Poison.encode!()

    send_resp(conn, 200, dashboards)
  end

  get "/:user/res" do
    is_reserved =
      conn.path_info
      |> List.first()
      |> UserReservartion.reserved?()
      |> Poison.encode!()

    send_resp(conn, 200, is_reserved)
  end

  post "/:user/res" do
    [user, _] = conn.path_info

    body =
      Map.get_lazy(conn.body_params, "_json", fn -> conn.body_params end)
      |> Map.put("user", user)

    %UserReservartion{}
    |> UserReservartion.changeset(body)
    |> Repo.insert()
    |> case do
      {:ok, _struct} -> send_resp(conn, 200, Poison.encode!(%{reserved: user}))
      {:error, _changeset} -> send_resp(conn, 400, Poison.encode!(%{error: "params not valid"}))
    end
  end
end
