defmodule Bashboard.Router do
  use Plug.Router

  plug(:match)
  plug(:dispatch)

  match("/all", to: Bashboard.Router.Admin)
  match(":user/", to: Bashboard.Router.User)
  match(":user/res", to: Bashboard.Router.User)
  match(":user/:dash/:widget/sse", to: Bashboard.Router.Widget)
  match(":user/:dash/:widget/settings", to: Bashboard.Router.Widget)
  match(":user/:dash/:widget", to: Bashboard.Router.Widget)
  match(":user/:dash", to: Bashboard.Router.Dash)
end
