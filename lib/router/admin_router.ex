defmodule Bashboard.Router.Admin do
  use Plug.Router
  alias Bashboard.Dash, as: Dash

  plug(:match)
  plug(:dispatch)

  get "/all" do
    Dash.get_unique_namespaces()
    |> Poison.encode!(pretty: true)
    |> (&send_resp(conn, 200, &1)).()
  end
end
