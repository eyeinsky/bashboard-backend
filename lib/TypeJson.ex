defmodule TypeJson do
  alias Ecto.Type

  @behaviour Ecto.Type
  @moduledoc """
  To store values that might be any json value.

  Not intended as a general replacement for :map.
  """

  def type, do: :json

  def cast(value) when is_nil(value), do: {:ok, value}
  def cast(value) when is_boolean(value), do: {:ok, value}
  def cast(value) when is_binary(value), do: {:ok, value}
  def cast(value) when is_number(value), do: {:ok, value}
  def cast(value) when is_list(value), do: Type.cast({:array, __MODULE__}, value)
  def cast(value) when is_map(value), do: Type.cast({:map, __MODULE__}, value)
  def cast(_), do: :error

  def load(data), do: {:ok, data}

  def dump(value) when is_nil(value), do: {:ok, value}
  def dump(value) when is_boolean(value), do: {:ok, value}
  def dump(value) when is_binary(value), do: {:ok, value}
  def dump(value) when is_number(value), do: {:ok, value}
  def dump(value) when is_list(value), do: Type.dump({:array, __MODULE__}, value)
  def dump(value) when is_map(value), do: Type.dump({:map, __MODULE__}, value)
  def dump(_), do: :error
end
