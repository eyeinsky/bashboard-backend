defmodule Bashboard.UserReservartion do
  use Ecto.Schema
  import Ecto.Query
  alias Bashboard.Repo, as: Repo
  alias Bashboard.UserReservartion, as: UserReservartion

  schema "user_reservations" do
    field(:user, :string, null: false)
    field(:email, :string, null: false)
    field(:updates_allowed, :boolean)
    field(:comments, :string)
    timestamps(type: :utc_datetime, updated_at: false)
  end

  def changeset(user_reservation, params) do
    user_reservation
    |> Ecto.Changeset.cast(params, [:user, :email, :updates_allowed, :comments])
    |> Ecto.Changeset.validate_required([:user, :email])
  end

  def reserved?(user) do
    from(s in UserReservartion,
      where: s.user == ^user
    )
    |> last()
    |> Repo.one()
    |> case do
      nil -> false
      _ -> true
    end
  end
end
