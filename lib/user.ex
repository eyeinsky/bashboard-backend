defmodule Bashboard.User do
  import Ecto.Query
  alias Bashboard.Repo, as: Repo

  @spec get_dashboards([bitstring]) :: list
  def get_dashboards([user]) do
    from(m in "messages_all_month",
      where: m.user == ^user,
      group_by: m.dash,
      select: m.dash
    )
    |> Repo.all()
  end
end
