defmodule Bashboard.Dash do
  import Ecto.Query
  alias Bashboard.Repo, as: Repo
  alias Bashboard.Widget, as: Widget
  alias Bashboard.Message, as: Message

  @spec get([bitstring]) :: map
  def get([user, dash]) do
    from(m in "messages_all_month",
      where: m.user == ^user and m.dash == ^dash,
      group_by: m.widget,
      select: m.widget
    )
    |> Repo.all()
    |> Map.new(fn widget -> {widget, Widget.get({user, dash, widget})} end)
  end

  def get_unique_namespaces() do
    from(m in Message,
      distinct: [m.user, m.dash, m.widget],
      select: {m.user, m.dash, m.widget}
    )
    |> Repo.all()
    |> Enum.map(fn {user, dash, widget} -> "/#{user}/#{dash}/#{widget}" end)
  end
end
