defmodule Bashboard.Options do
  @all_timeseries_single [
    {"last", ["l", "last"]},
    {"series", ["s", "ser", "series"]}
  ]

  @all_timeseries [
    {"minute", ["m", "min", "minute", "minutes"]},
    {"hour", ["h", "hour", "hours"]},
    {"day", ["d", "day", "days"]},
    {"month", ["m", "month", "months"]}
  ]

  @all_transforms %{
    :avg_numbers => ["a", "avg", "avg_num", "avg_numbers"],
    :sum_numbers => ["s", "sum", "sum_num", "sum_numbers"],
    :sum_map => ["sm", "sum_map"],
    :avg_map => ["am", "avg_map"],
    :count_messages => ["c", "count", "count_messages"],
    :categorize_strings => ["cat", "categorize", "categorize_strings"]
  }

  def get_all_combinations() do
    all_ts =
      for ts <- Map.keys(Map.new(@all_timeseries)),
          tf <- Map.keys(@all_transforms) do
        {ts, tf}
      end

    ts_s =
      @all_timeseries_single
      |> Enum.map(fn {key, _repr} -> {key, nil} end)

    ts_s ++ all_ts
  end

  def atomize_map(options) do
    for {key, val} <- options do
      case key do
        x when x in ["r", "raw"] -> :raw
        x when x in ["a", "all", "all_conf", "all_confs", "all_configurations"] -> :all_confs
        x when x in ["ti", "ts", "timeseries", "time"] -> {:timeseries, atomize_timeseries(val)}
        x when x in ["tr", "tf", "transform", "trans"] -> {:transform, atomize_transform(val)}
        _ -> nil
      end
    end
    |> Enum.filter(& &1)
    |> Enum.sort()
  end

  defp atomize_timeseries(val) do
    for {to, from_arr} <- Map.new(@all_timeseries_single ++ @all_timeseries),
        from <- from_arr,
        from == val do
      to
    end
    |> List.first()
  end

  defp atomize_transform(val) do
    for {to, from_arr} <- @all_transforms, from <- from_arr, from == val do
      to
    end
    |> List.first()
  end
end
