defmodule Bashboard.Helpers do
  def testing_mode?() do
    case Application.get_env(:bashboard, :environment) do
      :prod -> false
      :dev -> false
      _ -> true
    end
  end

  def bucket_to_timex(str) do
    case str do
      "second" -> :seconds
      "minute" -> :minutes
      "hour" -> :hours
      "day" -> :days
      "month" -> :months
    end
  end
end
