defmodule Bashboard.WidgetSettings do
  use Ecto.Schema
  import Ecto.Query
  alias Bashboard.Repo, as: Repo
  alias Bashboard.WidgetSettings, as: WidgetSettings

  schema "widget_settings" do
    field(:user, :string)
    field(:dash, :string)
    field(:widget, :string)
    field(:body, :map)
    timestamps(type: :utc_datetime, updated_at: false)
  end

  defp changeset(widget_settings, params) do
    widget_settings
    |> Ecto.Changeset.cast(params, [:user, :dash, :widget, :body])
    |> Ecto.Changeset.validate_required([:user, :dash, :widget, :body])
  end

  @spec insert({bitstring, bitstring, bitstring}, map) :: any
  def insert({user, dash, widget}, body) do
    body_stringified =
      Map.new(body, fn
        {name, val} when is_atom(val) -> {name, Atom.to_string(val)}
        other -> other
      end)

    %WidgetSettings{}
    |> changeset(%{user: user, dash: dash, widget: widget, body: body_stringified})
    |> Repo.insert()
  end

  @spec get({bitstring, bitstring, bitstring}) :: map
  def get({user, dash, widget}) do
    from(s in WidgetSettings,
      where: s.user == ^user and s.dash == ^dash and s.widget == ^widget
    )
    |> last()
    |> Repo.one()
  end
end
