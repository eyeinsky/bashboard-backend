defmodule Bashboard.MessageAggNumber do
  use Ecto.Schema

  schema "messages_agg_number" do
    field(:user, :string)
    field(:dash, :string)
    field(:widget, :string)
    field(:avg, :float)
    field(:sum, :float)
    field(:inserted_at, :utc_datetime)
  end
end
