defmodule Bashboard.MessageAggAll do
  use Ecto.Schema

  schema "messages_agg_all" do
    field(:user, :string)
    field(:dash, :string)
    field(:widget, :string)
    field(:count, :integer)
    field(:inserted_at, :utc_datetime)
  end
end
