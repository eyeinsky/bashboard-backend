defmodule Bashboard.Application do
  @moduledoc "OTP Application specification for Bashboard"

  use Application

  def start(_type, _args) do
    children = [
      Plug.Cowboy.child_spec(
        scheme: :http,
        plug: Bashboard.Endpoint,
        port: Application.get_env(:bashboard, :port),
        protocol_options: [idle_timeout: :infinity]
      ),
      {Bashboard.Repo, []},
      {Bashboard.Scheduler, []},
      {Bashboard.RunPeriodically, []},
      {PubSub, []}
    ]

    opts = [strategy: :one_for_one, name: Bashboard.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
