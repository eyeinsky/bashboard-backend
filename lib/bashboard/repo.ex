defmodule Bashboard.Repo do
  use Ecto.Repo,
    otp_app: :bashboard,
    adapter: Ecto.Adapters.Postgres

  use Ecto.Explain
end
