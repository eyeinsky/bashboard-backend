defmodule Bashboard.RunPeriodically do
  use GenServer

  def start_link(_) do
    GenServer.start_link(__MODULE__, %{})
  end

  @impl true
  def init(state) do
    if !Bashboard.Helpers.testing_mode?() do
      schedule_work()
    end

    {:ok, state}
  end

  @impl true
  def handle_info(:work, state) do
    Bashboard.ContinuousAggregator.all_once()

    # Reschedule once more
    schedule_work()

    {:noreply, state}
  end

  defp schedule_work do
    Process.send_after(self(), :work, 20 * 1000)
  end
end
