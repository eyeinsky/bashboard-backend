defmodule Bashboard.ContinuousAggregationTest do
  use ExUnit.Case
  use Plug.Test
  import Bashboard.MockData
  import Ecto.Query
  alias Bashboard.Repo, as: Repo

  setup :db_connection

  describe "Thousands of numbers." do
    setup [:mock_thousand_numbers]

    @tag timeout: :infinity
    test "Correctly bucketed." do
      for {ts, count_expected} <- [{"minute", 2881}, {"hour", 49}, {"day", 3}, {"month", 2}],
          type <- ["all", "numbers"] do
        table = "messages_#{type}_#{ts}"

        count_actual =
          from(m in table, select: count(m))
          |> Repo.one()

        assert count_actual === count_expected,
               "#{table} table failed with #{count_actual} actual === #{count_expected} expected rows."
      end
    end
  end
end
