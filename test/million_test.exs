defmodule Bashboard.MillionTest do
  use ExUnit.Case
  use Plug.Test
  import Bashboard.MockData

  @opts Bashboard.Endpoint.init([])
  @moduletag timeout: :infinity
  @moduletag :million

  setup :mock_million_maps

  test "Fetch all combinations from a million maps", %{namespace: namespace} do
    time_start = System.monotonic_time(:millisecond)

    conn =
      conn(:get, namespace <> "?all_configurations=true")
      |> Bashboard.Endpoint.call(@opts)

    time_spent = System.monotonic_time(:millisecond) - time_start

    assert conn.state == :sent
    assert conn.status == 200
    assert_in_delta time_spent, 150, 150
  end

  test "Sum a million maps", %{namespace: namespace} do
    time_start = System.monotonic_time(:millisecond)

    conn =
      conn(:get, namespace <> "?ts=minute&tr=sum_map")
      |> Bashboard.Endpoint.call(@opts)

    time_spent = System.monotonic_time(:millisecond) - time_start

    assert conn.state == :sent
    assert conn.status == 200
    assert_in_delta time_spent, 100, 100
  end

  test "Avg a million maps", %{namespace: namespace} do
    time_start = System.monotonic_time(:millisecond)

    conn =
      conn(:get, namespace <> "?ts=minute&tr=avg_map")
      |> Bashboard.Endpoint.call(@opts)

    time_spent = System.monotonic_time(:millisecond) - time_start

    assert conn.state == :sent
    assert conn.status == 200
    assert_in_delta time_spent, 75, 75
  end

  test "Count a million maps", %{namespace: namespace} do
    time_start = System.monotonic_time(:millisecond)

    conn =
      conn(:get, namespace <> "?ts=minute&tr=count")
      |> Bashboard.Endpoint.call(@opts)

    time_spent = System.monotonic_time(:millisecond) - time_start

    assert conn.state == :sent
    assert conn.status == 200
    assert_in_delta time_spent, 100, 100
  end
end
