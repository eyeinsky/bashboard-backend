defmodule Bashboard.MockData do
  require Ecto.Query
  import Ecto.Query
  alias Bashboard.Repo, as: Repo
  use ExUnit.Case
  use Plug.Test
  alias Bashboard.Message, as: Message

  def db_connection(_context \\ nil) do
    Ecto.Adapters.SQL.Sandbox.checkout(Repo)
    Ecto.Adapters.SQL.Sandbox.mode(Repo, {:shared, self()})
  end

  def mock_all(_context \\ nil) do
    mock_widget_numbers()
    mock_widget_numbers_many()
    mock_widget_numbers_gaps()
    mock_widget_numbers_dash2()
    mock_widget_strings()
    mock_widget_maps()
  end

  def mock_widget_numbers(_context \\ nil) do
    save_mocks_batch(["myuser", "mydash", "widget-numbers"], [60, 3.6, 10, 4])
  end

  def mock_widget_numbers_many(_context \\ nil) do
    save_mocks_batch(
      ["myuser", "mydash", "widget-numbers-many"],
      [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
    )
  end

  def mock_widget_numbers_gaps(_context \\ nil) do
    save_mocks_batch(["myuser", "mydash", "widget-numbers-gaps"], [1, 2, 3, 4, 5, 6, 7], 150)
  end

  def mock_widget_numbers_dash2(_context \\ nil) do
    save_mocks_batch(["myuser", "yourdash", "widget-numbers"], [9, 8, 7, 6])
  end

  def mock_widget_strings(_context \\ nil) do
    save_mocks_batch(["myuser", "mydash", "widget-strings"], [
      "foo",
      "bar",
      "baz",
      "bar",
      "foo",
      "foo"
    ])
  end

  def mock_widget_maps(_context \\ nil) do
    save_mocks_batch(["myuser", "mydash", "widget-maps"], [
      %{
        "action" => "sell",
        "price" => 50,
        "amount" => 3,
        "datetime" =>
          Timex.to_datetime({{2020, 1, 1}, {13, 14, 15}}, "Etc/UTC") |> Poison.encode!()
      },
      %{
        "action" => "buy",
        "price" => 48,
        "amount" => 8,
        "datetime" =>
          Timex.to_datetime({{2020, 1, 1}, {13, 15, 20}}, "Etc/UTC") |> Poison.encode!()
      },
      %{
        "action" => "sell",
        "price" => 49,
        "amount" => 5,
        "datetime" =>
          Timex.to_datetime({{2020, 1, 1}, {14, 16, 20}}, "Etc/UTC") |> Poison.encode!()
      },
      %{
        "action" => "sell",
        "price" => 51,
        "amount" => 4,
        "datetime" =>
          Timex.to_datetime({{2020, 1, 20}, {15, 19, 20}}, "Etc/UTC") |> Poison.encode!()
      },
      %{
        "action" => "sell",
        "price" => 49,
        "amount" => 11
      },
      %{
        "action" => "buy",
        "price" => 43
      }
    ])
  end

  def mock_million_maps(_context \\ nil) do
    from(m in Message,
      select: m,
      limit: 1
    )
    |> Repo.all()
    |> case do
      [%Message{}] ->
        nil

      [] ->
        IO.puts("Inserting million rows for testing (takes a few minutes).")

        {:ok, bench_pid} = Benchmark.start(:million_inserts)

        maps =
          Enum.map(1..1_000_000, fn index ->
            %{
              "index" => index,
              "price" => Enum.random(0..100)
            }
          end)

        save_mocks_batch(["myuser", "mydash", "million-maps"], maps)

        Benchmark.stop(bench_pid, :million_inserts)
    end

    %{namespace: "/myuser/mydash/million-maps"}
  end

  def mock_thousand_numbers(_context \\ nil) do
    save_mocks_batch(["myuser", "mydash", "thousand-numbers"], 1..5760)
  end

  defp save_mocks_batch(namespace, data, interval \\ 30) do
    [user, dash, widget] = namespace
    latest_dt = Timex.to_datetime({2020, 1, 1})

    data
    |> Enum.with_index()
    |> Enum.chunk_every(10000)
    |> (&Enum.chunk_every(&1, Enum.max([1, div(length(&1), 10)]))).()
    |> Enum.map(fn batches ->
      Task.async(fn ->
        for batch <- batches do
          batch
          |> Enum.map(fn {body, index} ->
            %{
              user: user,
              dash: dash,
              widget: widget,
              body: body,
              inserted_at:
                Timex.shift(latest_dt, seconds: -1 * interval * index)
                |> DateTime.truncate(:second)
            }
          end)
          |> (&Repo.insert_all(Message, &1)).()
        end
      end)
    end)
    |> Enum.map(fn task -> Task.await(task, :infinity) end)

    Bashboard.ContinuousAggregator.all_once()

    %{data: data, namespace: "/" <> Enum.join(namespace, "/")}
  end
end
