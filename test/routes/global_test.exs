defmodule Bashboard.GlobalTest do
  use ExUnit.Case
  use Plug.Test
  import Bashboard.MockData

  @opts Bashboard.Endpoint.init([])

  setup [:db_connection, :mock_all]

  test "Get all unique namespace paths" do
    conn =
      conn(:get, "/all")
      |> Bashboard.Endpoint.call(@opts)

    assert conn.state == :sent
    assert conn.status == 200

    assert [
             "/myuser/mydash/widget-maps",
             "/myuser/mydash/widget-numbers",
             "/myuser/mydash/widget-numbers-gaps",
             "/myuser/mydash/widget-numbers-many",
             "/myuser/mydash/widget-strings",
             "/myuser/yourdash/widget-numbers"
           ] == conn.resp_body |> Poison.decode!() |> Enum.sort()
  end
end
