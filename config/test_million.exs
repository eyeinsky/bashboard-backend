use Mix.Config

config :bashboard, Bashboard.Repo,
  database: "bashboard_test_million",
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  log: false

config :bashboard, ecto_repos: [Bashboard.Repo]

config :bashboard, port: 4002
