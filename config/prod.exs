use Mix.Config

config :logger,
  level: :debug,
  handle_otp_reports: true,
  handle_sasl_reports: true

config :bashboard, Bashboard.Repo,
  database: System.get_env("DB_NAME"),
  username: System.get_env("DB_USER"),
  password: System.get_env("DB_PASS"),
  hostname: System.get_env("DB_HOST")

config :bashboard, ecto_repos: [Bashboard.Repo]

config :bashboard, port: 4000

config :bashboard, Bashboard.Scheduler,
  debug_logging: false,
  jobs: [
    {{:extended, "*/1"}, {Bashboard.Dogfood, :system_info_running, []}},
    {"@reboot", {Bashboard.Dogfood, :system_info_once, []}}
  ]

config :bashboard, :environment, :prod
