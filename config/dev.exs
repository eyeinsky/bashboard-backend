use Mix.Config

config :bashboard, Bashboard.Repo,
  database: "bashboard_dev",
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  log: false

config :bashboard, ecto_repos: [Bashboard.Repo]

config :bashboard, port: 4001

config :bashboard, Bashboard.Scheduler,
  debug_logging: false,
  jobs: [
    {{:extended, "*/10"}, {Bashboard.Dogfood, :system_info_running, []}},
    {"@reboot", {Bashboard.Dogfood, :system_info_once, []}}
  ]

config :bashboard, :environment, :dev
